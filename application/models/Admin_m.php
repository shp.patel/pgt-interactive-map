<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_m extends My_Model {

    protected $_table_name     = 'tbl_admin';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
    protected $_timestamps     = TRUE;
 
    public function u_loggedin() {
        return (bool) $this->session->userdata('u_loggedin');
    }

    public function login($email, $password)
    {
        $this->db->where('password', $password);
    	$this->db->where('email', $email);
		$user = $this->db->get('tbl_admin')->row();
        if (count($user) )
        {
            if ($user->is_active == 1) 
            {
                $data = array(
                    'user_id' => $user->id,
                    'u_loggedin' => TRUE,
					'email' => $email,
					'name'=>$user->first_name.' '.$user->last_name
                );
    
                $this->session->set_userdata($data);
                return TRUE;
            }
        }
        return FALSE;
    }
}

