<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Map_m extends My_Model {

    protected $_table_name     = 'tbl_map_details';
    protected $_primary_key    = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by       = 'id';
 	protected $_timestamps     = TRUE;

     	public $rules = array(
			'community' => array(
				'field' => 'community',
				'label' => 'Community',
				'rules' => 'trim|required'
			),
			'region' => array(
				'field' => 'region',
				'label' => 'region',
				'rules' => 'trim'
			),
			'city' => array(
				'field' => 'city',
				'label' => 'city',
				'rules' => 'trim'
			),
			'builder_name' => array(
				'field' => 'builder_name',
				'label' => 'builder_name',
				'rules' => 'trim|required'
			),
			'dealer' => array(
				'field' => 'dealer',
				'label' => 'dealer',
				'rules' => 'trim'
			),
			'street' => array(
				'field' => 'street',
				'label' => 'street',
				'rules' => 'trim|required'
			),
			// 'pgt_product' => array(
			// 	'field' => 'pgt_product',
			// 	'label' => 'pgt product',
			// 	'rules' => 'trim|required'
			// ),
			'total_homes' => array(
				'field' => 'total_homes',
				'label' => 'total homes',
				'rules' => 'trim|numeric'
			),
			'empty_lots' => array(
				'field' => 'empty_lots',
				'label' => 'empty lots',
				'rules' => 'trim|numeric'
			),
			'total_pgt_homes' => array(
				'field' => 'total_pgt_homes',
				'label' => 'total pgt homes',
				'rules' => 'trim|numeric'
			),
			// 'impact_rate' => array(
			// 	'field' => 'impact_rate',
			// 	'label' => 'impact rate',
			// 	'rules' => 'trim|required|numeric|callback__maximumCheck'
			// ),
			// 'incumbent_competitor' => array(
			// 	'field' => 'incumbent_competitor',
			// 	'label' => 'incumbent competitor',
			// 	'rules' => 'trim|required'
			// ),
			'incumbent_rate' => array(
				'field' => 'incumbent_rate',
				'label' => 'incumbent rate',
				'rules' => 'trim|numeric|callback__maximumCheck'
			),
			'name' => array(
				'field' => 'name',
				'label' => 'name',
				'rules' => 'trim'
			),
			'contact_info' => array(
				'field' => 'contact_info',
				'label' => 'contact info',
					'rules' => 'trim'
			),
    	);

	
	function all($limit,$start,$col,$dir,$sWhere = '')
	{   
		$table = 'tbl_map_details';
		$relation = array(
			"fields" => "*",
			"conditions" => 'is_delete = 0'
		);
		if (strlen($sWhere) > 0) {
			$relation['conditions'] = $sWhere;
		}
		$relation['LIMIT'] = array(
			"start" => $limit,
			"end" => $start
		);
		$relation['ORDER_BY'] = array(
			"field" => $col,
			"order" => $dir
		);
		$contact_array = $this->get_relation($table, $relation);
		return $contact_array;
	}
}

