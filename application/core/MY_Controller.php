<?php
class MY_Controller extends CI_Controller 
{
	public $data = array();
		
		function __construct() 
		{
			parent::__construct();
			$this->data['errors'] = array();
			$this->load->model('admin_m');
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
		}
}
