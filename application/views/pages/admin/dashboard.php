<header id="page-header">
	<h1>Summary Section</h1>
</header>
<div id="content" class="padding-20">
	<div class="row">
		<!-- Feedback Box -->
		<div class="col-md-3 col-sm-6">
			<!-- BOX -->
			<div class="box danger"><!-- default, danger, warning, info, success -->
				<div class="box-title"><!-- add .noborder class if box-body is removed -->
					<h4><?= (isset($number_of_red_pins) && $number_of_red_pins <> 0) ? $number_of_red_pins : '0'?></h4>
					<small class="block">Number of Red Pins</small>
					<i class="fa fa-map"></i>
				</div>
				<div class="box-body text-center">
					<span class="sparkline" >
					</span>
				</div>
			</div>
			<!-- /BOX -->
		</div>
		<!-- Online Box -->
		<div class="col-md-3 col-sm-6">
			<!-- BOX -->
			<div class="box success"><!-- default, danger, warning, info, success -->
				<div class="box-title"><!-- add .noborder class if box-body is removed -->
					<h4><?= (isset($number_of_blue_pins) && $number_of_blue_pins <> 0) ? $number_of_blue_pins : '0'?></h4>
					<small class="block">Number of Blue Pins</small>
					<i class="fa fa-globe"></i>
				</div>
				<div class="box-body text-center">
					<span class="sparkline" >
					</span>
				</div>
			</div>
			<!-- /BOX -->
		</div>
		<div class="col-md-3 col-sm-6">
			<!-- BOX -->
			<div class="box info"><!-- default, danger, warning, info, success -->
				<div class="box-title"><!-- add .noborder class if box-body is removed -->
					<h4><?= (isset($average_impact_rate) && $average_impact_rate <> 0) ? number_format((float)$average_impact_rate, 2, '.', '') : '0.0'?> %</h4>
					<small class="block">Avgerage Impact Rate</small>
					<i class="fa fa-bar-chart-o"></i>
				</div>
				<div class="box-body text-center">
					<span class="sparkline" >
					</span>
				</div>
			</div>
			<!-- /BOX -->
		</div>
		<div class="col-md-3 col-sm-6">
			<!-- BOX -->
			<div class="box warning"><!-- default, danger, warning, info, success -->
				<div class="box-title"><!-- add .noborder class if box-body is removed -->
					<h4><?= (isset($average_incumbent_rate) && $average_incumbent_rate <> 0) ? number_format((float)$average_incumbent_rate, 2, '.', '') : '0.0'?> %</h4>
					<small class="block">Avgerage Incumbent Rate</small>
					<i class="fa fa-globe"></i>
				</div>
				<div class="box-body text-center">
					<span class="sparkline" >
					</span>
				</div>
			</div>
			<!-- /BOX -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div id="panel-2" class="panel panel-default">
				<div class="panel-heading">
					<span class="title elipsis">
						<strong>Average Incumbent Take</strong> <!-- panel title -->
					</span>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-bordered">
							<thead style="background-color: rgb(29, 137, 207); color: white;">
								<tr>
									<th>Index</th>
									<th>Region</th>
									<th>Average Incumbent Rate</th>
								</tr>
							</thead>
							<tbody>
								<?php if (count($region_wise_red_pins) > 0):?>
									<?php foreach($region_wise_red_pins as $key=>$value): ?>
										<tr>
											<td><?=$key+1?></td>
											<td><?=ucfirst($value['region']);?></td>
											<td><?=$value['avg_incumbent_rate'];?></td>
										</tr>
									<?php endforeach;?>
								<?php else:?>
									<tr><td colspan="3">No Records Found.</td></tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div id="panel-2" class="panel panel-default">
				<div class="panel-heading">
					<span class="title elipsis">
						<strong>Average Impact Rate </strong> <!-- panel title -->
					</span>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-hover table-bordered">
							<thead style="background-color: #e66454; color: white;">
								<tr>
									<th>Index</th>
									<th>Region</th>
									<th>Average Impact Rate</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($region_wise_blue_pins) > 0) :?>
									<?php foreach($region_wise_blue_pins as $key=>$value): ?>
										<tr>
											<td><?=$key+1?></td>
											<td><?=ucfirst($value['region']);?></td>
											<td><?=$value['avg_impact_rate'];?></td>
										</tr>
									<?php endforeach;?>
								<?php else:?>
									<tr><td colspan="3">No Records Found.</td></tr>
								<?php endif;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
