<!doctype html>
<html lang="en-US">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Smarty Admin</title>
		<meta name="description" content="" />
		<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />
		<!-- mobile settings -->
		<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
		<!-- WEB FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />
		<!-- CORE CSS -->
		<link href="<?=ASSET?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- THEME CSS -->
		<link href="<?=ASSET?>/css/essentials_admin.css" rel="stylesheet" type="text/css" />
		<link href="<?=ASSET?>/css/layout_admin.css" rel="stylesheet" type="text/css" />
		<link href="<?=ASSET?>/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	</head>
	<!--
		.boxed = boxed version
	-->
	<body>
		<div class="padding-15">
			<div class="login-box">
				<!-- login form -->
				<form id="login_form" action="<?=BASE_URL?>a_save" method="POST" class="sky-form boxed">
					<header><i class="fa fa-users"></i> Sign In</header>
					<fieldset>	
						<section>
							<label class="label">E-mail</label>
							<label class="input">
								<i class="icon-append fa fa-envelope"></i>
								<input type="email" name="email" id="email" value="admin@narola.email" >
								<span class="tooltip tooltip-top-right">Email Address</span>
							</label>
						</section>
						<section>
							<label class="label">Password</label>
							<label class="input">
								<i class="icon-append fa fa-lock"></i>
								<input type="password" name="password" id="password" value="narola21" >
								<b class="tooltip tooltip-top-right">Type your Password</b>
							</label>
							<!-- <label class="checkbox"><input type="checkbox" name="checkbox-inline" checked><i></i>Keep me logged in</label> -->
						</section>
					</fieldset>
					<footer>
						<button type="submit" class="btn btn-primary pull-right">Sign In</button>
					</footer>
				</form>
			</div>
		</div>
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = '<?=ASSET?>/plugins/';</script>
		<script type="text/javascript" src="<?=ASSET?>/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="<?=ASSET?>/js/app.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
		<script>        
			var site_url = "<?php echo site_url() ?>";
			var base_url = "<?php echo base_url() ?>";        
			var IMG = "<?php echo IMG ?>";        
			$(document).ready(function(){            
				var isset_error = "<?= $this->session->flashdata('flash_error'); ?>";
				var isset_sucess = "<?= $this->session->flashdata('flash_success'); ?>";
				var isset_warning = "<?= $this->session->flashdata('flash_warning'); ?>";
				if(isset_error !="")
					_toastr(isset_error,"top-right","error",false);
				if(isset_sucess !="")
					_toastr(isset_sucess,"top-right","success",false);
				if(isset_warning !="")
					_toastr(isset_warning,"top-right","info",false);  
				// validation
				$('#login_form').validate({
					rules: {
						email : { 
							required :true,
						},
						password : { 
							required :true,
						},
					},
					messages: {
						email : {
							required : 'Please enter valid email address',
						},
						password : {
							required : 'Please enter valid Password',
						},
					}
				});          
			});  
		</script>
	</body>
</html>
