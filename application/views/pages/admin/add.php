<header id="page-header">
	<h1>Contacts</h1>
	<ol class="breadcrumb">  
		<li><a href="<?=BASE_URL?>">Home</a></li>
		<li><a href="<?=BASE_URL?>a_contacts">Contacts</a></li>
		<li class="active"><?=(isset($result) && !empty($result->id) ) ? 'Edit' : 'Add'?></li>
	</ol>
</header>
<?php if (validation_errors()): ?>
	<div class="m-form__content">
		<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_msg" style="">
			<div class="m-alert__icon">
				<i class="la la-warning"></i>
			</div>
			<div class="m-alert__text">
				<?php echo validation_errors() ?>
			</div>
			<div class="m-alert__close">
				<button type="button" class="close" data-close="alert" aria-label="Close"></button>
			</div>
		</div>
	</div>
<?php endif; ?>

<div id="content" class="padding-20">
	<div class="row">
		<div class="col-md-12">
			<!-- ------ -->
			<!-- <div class="btn-wrap text-right">
				<a class="btn btn-success" href="<?=BASE_URL?>a_downloadView"> Refer Sample </a>
			</div> -->
			<small style="color:red">Note: Please check the validation data by reffering the rules. <a class="" href="<?=BASE_URL?>a_downloadView"> Click here </a> to check it.</small>
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-transparent">
					<strong><?=(isset($result) && !empty($result->id) ) ? 'EDIT' : 'ADD'?> CONTACT DETAILS</strong>
				</div>
				<div class="panel-body">
					<form id="add_contact_form" action="<?=BASE_URL?>a_contactSave" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!" data-toastr-position="top-right" novalidate="novalidate">
						<input type="hidden" name="id" value="<?=(isset($result) && !empty($result->id) ) ? $result->id : ''?>" name="id">
						<fieldset>
							<div class="row">
								<div class="form-group">
									<div class="col-md-3 col-sm-3">
										<label>Master Community: </label>
										<input type="text" name="master_community" value="<?=(isset($result) && !empty($result->master_community) ) ? $result->master_community : ''?>" id="master_community" class="form-control">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Community: *</label>
										<input type="text" name="community" value="<?=(isset($result) && !empty($result->community) ) ? $result->community : ''?>" id="community" class="form-control required">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Region: </label>
										<select name="region" id="region" class="form-control">
											<option value="">--Select Region--</option>
											<option value="southwest" <?=(isset($result) && !empty($result->region) &&  $result->region == 'southwest') ? 'selected' : ''?>>South West</option>
											<option value="southeast" <?=(isset($result) && !empty($result->region) &&  $result->region == 'southeast') ? 'selected' : ''?>>South East</option>
											<option value="centralwest" <?=(isset($result) && !empty($result->region) &&  $result->region == 'centralwest') ? 'selected' : ''?>>Central West</option>
											<option value="centraleast" <?=(isset($result) && !empty($result->region) &&  $result->region == 'centraleast') ? 'selected' : ''?>>Central East</option>
											<option value="northwest" <?=(isset($result) && !empty($result->region) &&  $result->region == 'northwest') ? 'selected' : ''?>>North West</option>
											<option value="northeast" <?=(isset($result) && !empty($result->region) &&  $result->region == 'northeast') ? 'selected' : ''?>>North East</option>
										</select>
										<!-- <input type="text" name="region" value="<?=(isset($result) && !empty($result->region) ) ? $result->region : ''?>" id="region" class="form-control required"> -->
									</div>
									<div class="col-md-3 col-sm-3">
										<label>City: </label>
										<input type="text" name="city" value="<?=(isset($result) && !empty($result->city) ) ? $result->city : ''?>" id="city" class="form-control ">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-3 col-sm-3">
										<label>Builder: *</label>
										<select name="builder_name" id="builder_name" class="form-control">
											<option value="">--Select Builder--</option>
											<option value="Pulte" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Pulte') ? 'selected' : ''?>>Pulte</option>
											<option value="Lennar" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Lennar') ? 'selected' : ''?>>Lennar</option>
											<option value="Khov" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Khov') ? 'selected' : ''?>>Khov</option>
											<option value="DR Horton" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'DR Horton') ? 'selected' : ''?>>DR Horton</option>
											<option value="Toll Brothers" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Toll Brothers') ? 'selected' : ''?>>Toll Brothers</option>
											<option value="GL Homes" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'GL Homes') ? 'selected' : ''?>>GL Homes</option>
											<option value="Minto" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Minto') ? 'selected' : ''?>>Minto</option>
											<option value="Stock" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Stock') ? 'selected' : ''?>>Stock</option>
											<option value="Kolter" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Kolter') ? 'selected' : ''?>>Kolter</option>
											<option value="Centerline-Tapestry" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Centerline-Tapestry') ? 'selected' : ''?>>Centerline-Tapestry</option>
											<option value="CC Homes" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'CC Homes') ? 'selected' : ''?>>CC Homes</option>
											<option value="David Weekley" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'David Weekley') ? 'selected' : ''?>>David Weekley</option>
											<option value="DM Dean" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'DM Dean') ? 'selected' : ''?>>DM Dean</option>
											<option value="Atlantic Crystals" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'Atlantic Crystals') ? 'selected' : ''?>>Atlantic Crystals</option>
											<option value="SCI" <?=(isset($result) && !empty($result->builder_name) &&  $result->builder_name == 'SCI') ? 'selected' : ''?>>SCI</option>
										</select>
										<!-- <input type="text" name="builder_name" value="<?=(isset($result) && !empty($result->builder_name) ) ? $result->builder_name : ''?>" id="builder_name" class="form-control required"> -->
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Dealer: </label>
										<input type="text" name="dealer" value="<?=(isset($result) && !empty($result->dealer) ) ? $result->dealer : ''?>" id="dealer" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Street: *</label>
										<input type="text" name="street" value="<?=(isset($result) && !empty($result->street) ) ? $result->street : ''?>" id="street" class="form-control required" onkeypress="return blockSpecialChar(event)">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Available PGT Product: </label>
										<input type="text" name="pgt_product" value="<?=(isset($result) && !empty($result->pgt_product) ) ? $result->pgt_product : ''?>" id="pgt_product" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-3 col-sm-3">
										<label>Total Homes: </label>
										<input type="number" name="total_homes" value="<?=(isset($result) ) ? $result->total_homes : ''?>" id="total_homes" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Empty Lots: </label>
										<input type="number" name="empty_lots" value="<?=(isset($result) ) ? $result->empty_lots : ''?>" id="empty_lots" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Total PGT Homes: </label>
										<input type="number" name="total_pgt_homes" value="<?=(isset($result)) ? $result->total_pgt_homes : ''?>" id="total_pgt_homes" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Incumbent Competitor: </label>
										<input type="text" name="incumbent_competitor" value="<?=(isset($result) && !empty($result->incumbent_competitor) ) ? $result->incumbent_competitor : ''?>" id="incumbent_competitor" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-3 col-sm-3">
										<label>Impact Rate: </label>
										<input type="number" name="impact_rate" value="<?=(isset($result) ) ? $result->impact_rate : ''?>" id="impact_rate" class="form-control ">
									</div>
									
									<div class="col-md-3 col-sm-3">
										<label>Onsite Collateral: </label>
										<label class="radio">
											<input type="radio" name="onsite_collateral" value="1" <?=(isset($result) && $result->onsite_collateral == 1) ? 'checked' : ''?>>
											<i></i> Yes
										</label>
										<label class="radio">
											<input type="radio" name="onsite_collateral" value="0" <?=(isset($result) && $result->onsite_collateral == 0) ? 'checked' : ''?>>
											<i></i> No
										</label>
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Trained: </label>
										<label class="radio">
											<input type="radio" name="trained" value="1" <?=(isset($result) && $result->trained == 1) ? 'checked' : ''?>>
											<i></i> Yes
										</label>
										<label class="radio">
											<input type="radio" name="trained" value="0" <?=(isset($result) && $result->trained == 0) ? 'checked' : ''?>>
											<i></i> No
										</label>
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Last Action: </label>
										<input type="text" name="last_action" id="last_action"  value="<?=(isset($result) && !empty($result->last_action) ) ? $result->last_action : ''?>" class="form-control datepicker" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" >
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-3 col-sm-3">
										<label>Incumbent Rate: </label>
										<input type="number" name="incumbent_rate" value="<?=(isset($result)) ? $result->incumbent_rate : ''?>" id="incumbent_rate" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Name: </label>
										<input type="text" name="name" value="<?=(isset($result) && !empty($result->name) ) ? $result->name : ''?>" id="name" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Contact Info: </label>
										<input type="text" name="contact_info" value="<?=(isset($result) && !empty($result->contact_info) ) ? $result->contact_info : ''?>" id="contact_info" class="form-control ">
									</div>
									<div class="col-md-3 col-sm-3">
										<label>Sunset: </label>
										<label class="radio">
											<input type="radio" name="sunset" value="1" <?=(isset($result) && $result->sunset == 1) ? 'checked' : ''?>>
											<i></i> Yes
										</label>
										<label class="radio">
											<input type="radio" name="sunset" value="0" <?=(isset($result) && $result->sunset == 0) ? 'checked' : ''?>>
											<i></i> No
										</label>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-3 col-sm-3">
										<label>County: </label>
										<input type="text" name="county" value="<?=(isset($result)) ? $result->county : ''?>" id="county" class="form-control">
									</div>
								</div>
							</div>
						</fieldset>
						<div class="row">
							<div class="col-md-12 text-center btn-wrap">
								<button type="reset" class="btn btn-3d  btn-danger reset-btn">
									RESET
								</button>
								<button type="submit" class="btn btn-3d btn-success">
									SUBMIT
								</button>
							</div>
						</div>
						<input type="hidden" name="is_ajax" value="true">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){  
			$.validator.addMethod("alpha", function(value, element) {
				return this.optional(element) || value == value.match(/^['\-\\%_#/!()=+/a-z@A-Z\s]+$/);
			});
			$.validator.addMethod("alphadot", function(value, element) {
				return this.optional(element) || value == value.match(/^[a-zA-Z.\s]+$/);
			});
			$.validator.addMethod("custom", function(value, element) {
				return this.optional(element) || value == value.match(/^[a-z@A-Z|\s]+$/);
			});
			$.validator.addMethod("alphanumeric", function(value, element) {
				return this.optional(element) || value == value.match(/^[0-9@.a-zA-Z|\s]+$/);
			});
			// $.validator.addMethod("email_contact_number", function(value, element) {
			// 	return this.optional(element) || value == value.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/);
			// });
			$('#add_contact_form').validate({
			rules: {
				master_community :{
					alpha : true
				},
				community : { 
					required :true,
					alpha : true
				},
				region : { 
					// required :true,
					alpha : true
				},
				county : { 
					// required :true,
					alpha : true
				},
				city : { 
					// required :true,
					alphadot : true
				},
				builder_name : { 
					required :true,
					custom : true
				},
				dealer : { 
					// required :true,
					custom : true
				},
				street : { 
					required :true,
					alphanumeric : true
				},
				pgt_product : { 
					alpha :true,
				},
				total_homes : { 
					// required :true,
					digits: true
				},
				empty_lots : { 
					// required :true,
					digits: true
				},
				total_pgt_homes : { 
					// required :true,
					digits: true
				},
				impact_rate : { 
					range: [0, 100],
					digits: true
				},
				incumbent_competitor : { 
					alpha : true
				},
				incumbent_rate : { 
					// required :true,
					range: [0, 100],
					digits: true
				},
				name : { 
					// required :true,
					alpha : true
				},
				contact_info : { 
					// required :true,
					// email_contact_number : true
				},
				last_action : { 
					// required :true,
				},
			},
			messages: {
				master_community :{
					alpha : 'This field contains only character and spaces'
				},
				community : {
					required : 'Please enter valid Community',
					alpha : 'This field contains only character and spaces'
				},
				region : {
					// required : 'Please select any Region',
					alpha : 'This field contains only character and spaces'
				},
				county : {
					// required : 'Please enter valid county',
					alpha : 'This field contains only character and spaces'
				},
				city : {
					// required : 'Please enter valid City',
					alphadot : 'This field contains only character,dot and spaces'
				},
				builder_name : {
					required : 'Please enter valid Builder-name',
					custom : 'Please use | instead of , to add more than one item. e.g. Builder1 | Buiilder2 | Builder3'
				},
				dealer : {
					// required : 'Please enter valid Dealer',
					// alpha : 'This field contains only character and spaces',
					custom : 'Please use | instead of , to add more than one item. e.g. Dealer1 | Dealer2 | Dealer3'
				},
				street : {
					required : 'Please enter valid Street',
					alphanumeric : 'This field contains only character, digits and spaces'
				},
				pgt_product : {
					alpha : 'This field contains only character and spaces'
				},
				total_homes : {
					// required : 'Please enter valid Total homes',
					digits: "Please enter valid positive value"
				},
				empty_lots : {
					// required : 'Please enter valid Empty lots',
					digits: "Please enter valid positive value"
				},
				total_pgt_homes : {
					// required : 'Please enter valid Total PGT homes',
					digits: "Please enter valid positive value"
				},
				impact_rate : {
					range: 'Enter valid number between 0-100',
					digits: "Please enter valid positive value"
				},
				incumbent_competitor : {
					alpha : 'This field contains only character and spaces'
				},
				incumbent_rate : {
					// required : 'Please enter valid Incumbent rate',
					range: 'Enter valid number between 0-100',
					digits: "Please enter valid positive value"
				},
				name : {
					// required : 'Please enter valid Name',
					alpha : 'This field contains only character and spaces'
				},
				contact_info : {
					// required : 'Please enter valid Contact Info',
					// email_contact_number : 'Email Address / Phone number is not valid, Please provide a valid Email or phone number'
				},
				last_action : {
					// required : 'Please select date',
				},
			}
		});    
	});
	function blockSpecialChar(e){
		var k;
		document.all ? k = e.keyCode : k = e.which;
		return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
	}
	$(document).on('click','.reset-btn', function()
	{
		var validator = $( "#add_contact_form" ).validate();
		validator.resetForm();
	});
</script>
