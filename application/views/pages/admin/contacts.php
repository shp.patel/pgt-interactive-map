<header id="page-header">
	<h1>Contacts</h1>
	<ol class="breadcrumb">
		<li><a href="<?=BASE_URL?>">Home</a></li>
		<li class="active">Contacts</li>
	</ol>
</header>
<!--  -->
<?php if (validation_errors()): ?>
	<div class="m-form__content">
		<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_msg" style="">
			<div class="m-alert__icon">
				<i class="la la-warning"></i>
			</div>
			<div class="m-alert__text">
				<?php echo validation_errors() ?>
			</div>
			<div class="m-alert__close">
				<button type="button" class="close" data-close="alert" aria-label="Close"></button>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="display_errors" style="display:none">
	<div class="m-alert m-alert--icon alert alert-danger" role="alert" id="m_form_msg" style="">
		<div class="m-alert__icon">
			<i class="la la-warning"></i>
		</div>
		<div>
			<pre class="error_message" >
			</pre>
		</div>
		<div class="m-alert__close">
			<button type="button" class="close" data-close="alert" aria-label="Close"></button>
		</div>
	</div>
</div>
<div id="panel-5" class="panel panel-default">
	<div class="panel-heading">
		<span class="title elipsis">
			<strong>CONTACT LISTING</strong> <!-- panel title -->
		</span>
		<!-- right options -->
		<ul class="options pull-right list-inline">
			<li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
			<li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
			<li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
		</ul>
		<!-- /right options -->
	</div>
	<!-- panel content -->
	<div class="panel-body">
		<div class="btn-wrap text-right">
			<a class="btn btn-info" href="<?=BASE_URL?>a_add"> Add Contact </a>
			<a class="btn btn-success" href="<?=BASE_URL?>a_downloadView"> Download Sample </a>
			<a class="btn btn-default" href="#" id="import_btn"> Import</a>
		</div>
		<input type="file" name="import_file" id="import_file" style="display:none">
		<table class="table table-striped table-bordered table-hover" id="sample_5">
			<thead>
				<tr>
					<th>Master Community</th>
					<th>Community</th>
					<th>Region</th>
					<th>City</th>
					<th>Builder</th>
					<th>Dealer</th>
					<th>Street</th>
					<th>PGT Product</th>
					<th>Total Home</th>
					<th>Empty Slots</th>
					<th>Total PGT homes</th>
					<th>Impact Rate</th>
					<th>Incumbent Competitor</th>
					<th>Incumbent Rate</th>
					<th>Onsite Collateral</th>
					<th>Trained</th>
					<th>Name</th>
					<th>Contact Info</th>
					<th>Last Action</th>
					<th>Sunset</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	loadScript(plugin_path + "datatables/js/jquery.dataTables.min.js", function(){
		loadScript(plugin_path + "datatables/js/dataTables.tableTools.min.js", function(){
			loadScript(plugin_path + "datatables/js/dataTables.colReorder.min.js", function(){
				loadScript(plugin_path + "datatables/js/dataTables.scroller.min.js", function(){
					loadScript(plugin_path + "datatables/dataTables.bootstrap.js", function(){
						loadScript(plugin_path + "select2/js/select2.full.min.js", function(){
							if (jQuery().dataTable) {
								function initTable() {
									var table = jQuery('#sample_5');
									var oTable = table.dataTable({
										"scrollX": true,
										"order": [
											[0, 'asc']
										],
										"pageLength": 10, // set the initial value,
										"processing": true,
										"destroy"   : true,
        								"serverSide": true,
										"ajax":{
											"url": "<?= BASE_URL?>a_contactsjson",
											"dataType": "json",
											"type": "POST",
											"data":{}
										},  
										"columns": [
											{
												"data": "master_community",
											},
											{
												"data": "community",
											},
											{
												"data": "region" 
											},
											{
												"data": "city",
											},
											{
												"data": "builder_name",
											},
											{
												"data": "dealer",
											},
											{
												"data": "street",
											},
											{
												"data": "pgt_product",
											},
											{
												"data": "total_homes",
											},
											{
												"data": "empty_lots",
											},
											{
												"data": "total_pgt_homes",
											},
											{
												"data": "impact_rate",
											},
											{
												"data": "incumbent_competitor",
											},
											{
												"data": "incumbent_rate",
											},
											{
												"data": "onsite_collateral",
												render: function (data, type, full, meta) {                    
													if(full.onsite_collateral == 1){
														label_color = 'bg-info';
														label_title = 'Yes';
													}else{                       
														label_color = 'bg-danger';
														label_title = 'No';
													}      
													return '<label class="label '+label_color+'">'+label_title+'</label>';
												}
											},
											{
												"data": "trained",
												render: function (data, type, full, meta) {                    
													if(full.trained == 1){
														label_color = 'bg-info';
														label_title = 'Yes';
													}else{                       
														label_color = 'bg-danger';
														label_title = 'No';
													}      
													return '<label class="label '+label_color+'">'+label_title+'</label>';
												}
											},
											{
												"data": "name",
											},
											{
												"data": "contact_info",
											},
											{
												"data": "last_action",
												render: function (data, type, full, meta) {                    
													var d = new Date(full.last_action),
														month = '' + (d.getMonth() + 1),
														day = '' + d.getDate(),
														year = d.getFullYear();
													console.log("dfssd",full.last_action)
													if (month.length < 2) month = '0' + month;
													if (day.length < 2) day = '0' + day;
													return [month,day,year].join('-');
												}
											},
											{
												"data": "sunset",
												render: function (data, type, full, meta) {                    
													if(full.sunset == 1){
														label_color = 'bg-info';
														label_title = 'Yes';
													}else{                       
														label_color = 'bg-danger';
														label_title = 'No';
													}      
													return '<label class="label '+label_color+'">'+label_title+'</label>';
												}
											},
										] ,
										"columnDefs": [ {
											"targets": 20,
											"data": "id",
											"orderable": false,
											"visible" : true,
											"render": function ( data, type, row, meta ) {
												var record = row.id;
												btn = '<a href="'+site_url+'a_contactEdit/'+record+'" class="btn btn-sm btn-icon btn-primary btn-rounded	position-left" title="Edit"><i class="fa fa-pencil"></i></a>';
												btn += '<button type="button" id="delete_btn" data-customid="'+record+'" class="btn btn-sm btn-icon btn-danger" title="Delete"><i class="fa fa-trash"></i></button>';                    
												return btn;     
											}
										} ] ,
									});
									var oTableColReorder = new jQuery.fn.dataTable.ColReorder( oTable );
									var tableWrapper = jQuery('#sample_5_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
									tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
								}
								initTable();


								$(document).on('click', '#delete_btn', function() {
									var customid = $(this).attr("data-customid");
									swal({
										title: 'Are you sure?',
										text: "You would like to delete this record.",
										type: 'warning',
										showCancelButton: true,
										confirmButtonColor: '#3085d6',
										cancelButtonColor: '#d33',
										confirmButtonText: 'Yes, delete it!'
									}).then(function (result) {
										if( result == true){
											jQuery.ajax({
												url : '<?php echo BASE_URL?>admin/contacts/delete',
												method: 'post',
												dataType: 'json',
												data: {id : customid},
												success: function(response){
													if (response.success == true)
													{
														initTable();
														_toastr(response.message,"top-right","success",false);
														swal("success", response.message, "success");
													}
													else
													{
														initTable();
														_toastr(response.message,"top-right","error",false);
														swal("error", response.message, "error");
													}
												}
											});
										}
									})
								});

								$("#import_btn").click(function() {
									$("input[name=import_file]").trigger('click');
								});

								$(document).on("change","#import_file",function()
								{
									var result = [];
									var fileUpload = document.getElementById("import_file");
									var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv)$/;
									if (regex.test(fileUpload.value.toLowerCase())) 
									{
										if (typeof (FileReader) != "undefined") 
										{
											var reader = new FileReader();
											reader.onload = function (e) 
											{
												var table = document.createElement("table");
												var rows = e.target.result.split("\n");
												console.log(rows);
												if (rows.length > 0)
												{
													for (var i = 0; i < rows.length; i++) {
														result.push(rows[i]);
													}
													$.ajax({
														url: "<?= BASE_URL?>admin/contacts/import_csv",
														type: 'POST',
														data: {data:result},
														beforeSend: function() {
															$("#loader").show();
														},
														success: function(response) {
															initTable();
															$("#loader").hide();
															if (response.errors === "")
																_toastr("CSV file imported successfully","top-right","success",false);
															else
															{
																$(".error_message").html(response.errors);
																_toastr("Please check the error and try to upload again","top-right","error",false);
																$(".display_errors").show();
															}
														}
													});
												}
											}
											reader.readAsText(fileUpload.files[0]);
										} else {
											alert("This browser does not support HTML5.");
										}
									} else {
										alert("Please upload a valid CSV file.");
									}
								});
							}
						});
					});
				});
			});
		});
	});
</script>
