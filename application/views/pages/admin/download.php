<header id="page-header">
	<h1>Columns detail with validation and example</h1>
	<ol class="breadcrumb">
		<li><a href="<?=BASE_URL?>">Home</a></li>
		<li><a href="<?=BASE_URL?>a_contacts">Contacts</a></li>
		<li class="active">Downlaod Sample></li>
	</ol>
</header>
<div id="content" class="padding-20">
	<div class="row">
		<div class="col-md-12">
			<div class="text-right btn-wrap">
				<a class="btn btn-success" href="<?=BASE_URL?>a_download"> Download Sample</a>
			</div>
			<small style="color:red">Note: These information helps you to fill up the CSV file with data validation.</small>
			<table class="table table-bordered table-responsive">
				<thead>
					<th>Column</th>
					<th>Validation</th>
					<th>Example</th>
				</thead>
				<tbody>
					<tr>
						<td>Master Community (If applicable)</td>
						<td>Text(allow @ character). This may be Not Applicable</td>
						<td>Palmer Ranch </td>
					</tr>
					<tr>
						<td>Community Name</td>
						<td>Text(allow @ character). There may be several communities in a master community.</td>
						<td>Prestancia</td>
					</tr>
					<tr>
						<td>Region (also called Territory or District or Division)</td>
						<td>Text.Special characters like !@#$%^&*().<>?:"{}_+ not allowed</td>
						<td>Southwest</td>
					</tr>
					<tr>
						<td>City</td>
						<td>Text.Special characters like !@#$%^&*().<>?:"{}_+ not allowed</td>
						<td>Sarasota</td>
					</tr>
					<tr>
						<td>Builder Name</td>
						<td>Each builder will have several communities that they are working in. Should be | seperated(comma is not working)</td>
						<td>Builder1 | builder2</td>
					</tr>
					<tr>
						<td>Dealer(s)</td>
						<td>There may be several Dealers for each Community.Dealers will be working in multiple communities. Should be | seperated(comma is not working)</td>
						<td>dealer1 | dealer2 | dealer3</td>
					</tr>
					<tr>
						<td>Community Street Address</td>
						<td>Address. Special characters like !@#$%^&*().<>?:"{}_+ not allowed</td>
						<td>8310 Lakewood Main Street Sarasota FL 34275</td>
					</tr>
					<tr>
						<td>Applicable PGT Product</td>
						<td>Text.This may be Not Applicable </td>
						<td>High Impact</td>
					</tr>
					<tr>
						<td>Total Number of Homes</td>
						<td>numeric</td>
						<td>300</td>
					</tr>
					<tr>
						<td>Empty Lots</td>
						<td>numeric</td>
						<td>100</td>
					</tr>
					<tr>
						<td>Number of homes using PGT Product</td>
						<td>numeric</td>
						<td>100</td>
					</tr>
					<tr>
						<td>Impact Take Rate</td>
						<td>percentage.This may be Not Applicable</td>
						<td>78</td>
					</tr>
					<tr>
						<td>Incumbent Competitor</td>
						<td>Text.This may be Not Applicable</td>
						<td>anyname</td>
					</tr>
					<tr>
						<td>Incumbent Take Rate</td>
						<td>percentage</td>
						<td>12</td>
					</tr>
					<tr>
						<td>On Site Collateral?</td>
						<td>Y or N</td>
						<td>Y</td>
					</tr>
					<tr>
						<td>Trained?</td>
						<td>Y or N</td>
						<td>Y</td>
					</tr>
					<tr>
						<td>Contact Name</td>
						<td>Text</td>
						<td>name</td>
					</tr>
					<tr>
						<td>Contact Info (Tel, Email)</td>
						<td>Text</td>
						<td>johndickenson@gmail.com  (941) 408-3793</td>
					</tr>
					<tr>
						<td>Last Action</td>
						<td>Date</td>
						<td>mm-dd-yyyy</td>
					</tr>
					<tr>
						<td>Sunset?</td>
						<td>Y or N</td>
						<td>N</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
