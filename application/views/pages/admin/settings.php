<header id="page-header">
	<h1>Settings</h1>
	<ol class="breadcrumb">
		<li><a href="#">Settings</a></li>
		<li class="active">Change Password</li>
	</ol>
</header>
<div id="content" class="padding-20">
	<div class="row">
		<div class="col-md-6">
			<!-- ------ -->
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-transparent">
					<strong>CHANGE PASSWORD</strong>
				</div>
				<div class="panel-body">
					<form id="chnage_password_form" action="<?=BASE_URL?>a_changePassword" method="post" enctype="multipart/form-data" data-success="Sent! Thank you!" data-toastr-position="top-right" novalidate="novalidate">
						<fieldset>
							<!-- required [php action request] -->
							<input type="hidden" name="action" value="contact_send">
							<div class="row">
								<div class="form-group">
									<div class="col-md-12 col-sm-12">
										<label>New Password: *</label>
										<input type="password" name="new_password" id="new_password" value="" class="form-control required">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group">
									<div class="col-md-12 col-sm-12">
										<label>Comfirm Password: *</label>
										<input type="password" name="confirm_password" id="confirm_password" value="" class="form-control required">
									</div>
								</div>
							</div>
						</fieldset>
						<div class="row">
							<div class="col-md-12 margin-top-20 text-right">
								<button type="reset" class="btn btn-3d btn-danger ">
									RESET
								</button>
								<button type="submit" class="btn btn-3d btn-success ">
									CHANGE PASSWORD
								</button>
							</div>
						</div>
						<input type="hidden" name="is_ajax" value="true">
					</form>
				</div>
			</div>
			<!-- /----- -->
		</div>
	</div>
</div>
<script>
		$(document).ready(function(){  
			$('#chnage_password_form').validate({
			rules: {
				new_password : { 
					required :true,
				},
				confirm_password : { 
					required :true,
					equalTo: "#new_password"
				},
			},
			messages: {
				new_password : {
					required : 'Please enter valid password',
				},
				confirm_password : {
					required : 'Please enter valid confirm Password',
					equalTo : 'Your password and confirm password does not match'
				},
			}
		});    
	});
</script>
