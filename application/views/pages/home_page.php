<nav class="navbar">
  <div class="container-fluid">
	<div class="d-flex">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			</button>
			<a class="navbar-brand" href="#"><img src="<?=ASSET?>images/pgti-logo-lg.jpg" alt="admin panel" height="60" /></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
					<li><a href="<?=BASE_URL?>a_login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			</ul>
		</div>


    <!-- <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
    		<li><a href="<?=BASE_URL?>a_login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div> -->
  </div>
</nav>
<div class="row">
	<div class="col-lg-2 col-md-3 col-sm-12 p-0">
		<div class="sidebar">
			<div class="fillters">
				<h2>Fillters:</h2>
					<div class="fillter-wrap">
						<button data-toggle="collapse" data-target="#impact-demo"><h4>Based on Impact Rate <i class="fa fa-angle-down"></i></h4></button>
						<div class="collapse in" id="impact-demo">
							<div class="chckbox-inner-wrap">
								<label for='first' class="custom-checkbox">0-20 %
									<input type="checkbox" name="filter" id="first" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="chckbox-inner-wrap">
								<label for='second' class="custom-checkbox">21-40 %
									<input type="checkbox" name="filter" id="second" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="chckbox-inner-wrap">
								<label for='third' class="custom-checkbox">41-60 %
									<input type="checkbox" name="filter" id="third" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="chckbox-inner-wrap">
								<label for='fourth' class="custom-checkbox">61-80 %
									<input type="checkbox" name="filter" id="fourth" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="chckbox-inner-wrap">
								<label for='fifth' class="custom-checkbox">81-100 %
									<input type="checkbox" name="filter" id="fifth" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>	
						</div>
					</div>
					<div class="fillter-wrap">
						<!-- <h4>Based on PGT</h4> -->
						<button data-toggle="collapse" data-target="#pgt-demo"><h4>Based on PGT <i class="fa fa-angle-down"></i></h4></button>
						<div class="collapse" id="pgt-demo">
							<div class="chckbox-inner-wrap">
								<label for='red' class="custom-checkbox">No PGT Presence
									<input type="checkbox" name="filter" id="red" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>
							<div class="chckbox-inner-wrap">
								<label for='blue' class="custom-checkbox">PGT Presence
									<input type="checkbox" name="filter" id="blue" class='chk-btn'>
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="fillter-wrap">
						<button data-toggle="collapse" data-target="#demo"><h4>Based on Builder <i class="fa fa-angle-down"></i></h4></button>
						<div class="collapse" id="demo">
							<?php foreach($builders as $builder):?>
								<div class="chckbox-inner-wrap">
									<label for='<?=$builder['builder_name']?>' class="custom-checkbox"><?=$builder['builder_name']?><span>   [<?=$builder['total']?>]</span>
										<input type="checkbox" name="filter" id="<?=$builder['builder_name']?>" class='chk-btn'>
										<span class="checkmark"></span>
									</label>
								</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>
				<button class="btn btn-danger pins-btn" >Hide pins</button>
				<!-- <button class="btn btn-success" onClick="toggleMarkers()">Hide pins</button> -->
			</div>
		</div>
		<div class="col-lg-10 col-md-9 col-sm-12 p-0">
			<div class="">
				<div id="map" style=""></div>
			</div>
		</div>
	</div>
</div>


