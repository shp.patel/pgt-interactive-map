<!DOCTYPE html>
<html>
	<head>    
		<?php $this->load->view('layouts/wb_head');?>
	</head>
	<noscript>
	</noscript>
	<body class="smoothscroll enable-animation">
		<div id="wrapper">
			<?php echo $body; ?>   
		</div>
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = '<?= ASSET; ?>plugins/';</script>    
		<script type="text/javascript" src="<?= ASSET; ?>js/scripts.js"></script>
		<!-- STYLESWITCHER - REMOVE -->
		<script async type="text/javascript" src="<?= ASSET; ?>plugins/styleswitcher/styleswitcher.js"></script>
		<!-- PAGE LEVEL SCRIPTS -->
		<script type="text/javascript" src="<?= ASSET; ?>js/view/demo.shop.js"></script>
		<script 
			src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLE_API_KEY?>">
		</script>
		<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js"></script>
		<script>
			// step 1: fetch the records from the database and mark as the marker
			google.maps.event.addDomListener(window, "load", fetchRecords);
			// use by on load event to get the data
			function fetchRecords()
			{
				$.ajax({
					url: "<?=BASE_URL?>home/fetchRecord",
					async: true,
					dataType: 'json',
					success: function (data) {
						map = initMap(); // initialize the map
						renderMap(data.red,"red", map); // render the red pins
						renderMap(data.blue, "blue",map); // render the blue pins
					}
				});  
			}
			
			// initializa the map and create the Map instance
			// display all KML layers here
			function initMap()
			{
				var center_point = {lat: 28.994402 , lng:-84.760254};
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 8,
					center: center_point,
				});
				google.maps.event.addListenerOnce(map, 'idle', function(){
					jQuery('.gm-style-iw').prev('div').remove();
				}); 
				var ctaLayer1 = new google.maps.KmlLayer({
					url: 'https://rdmedialabs.com/PGT/assets/kml_layer.kml?12',
					map: map,
					clickable: false,
				});
				var ctaLayer2 = new google.maps.KmlLayer({
					url: 'https://rdmedialabs.com/PGT/assets/lee.kml?2',
					map: map,
					clickable: false,
				});
				var ctaLayer3 = new google.maps.KmlLayer({
					url: 'https://rdmedialabs.com/PGT/assets/monroe.kml?3',
					map: map,
					clickable: false,
				});
				var ctaLayer4 = new google.maps.KmlLayer({
					url: 'https://rdmedialabs.com/PGT/assets/brevard.kml?4',
					map: map,
					clickable: false,
				});
				var ctaLayer5 = new google.maps.KmlLayer({
					url: 'https://rdmedialabs.com/PGT/assets/volusia.kml?5',
					clickable: false,
					map: map
				});
				return map;
			}

			// method use to place the marker based on the color
			var gmarkers = [];
			// var filters;
			// var filters = {first:false, second:false, third:false,fourth:false,fifth:false, red:false, blue:false, Pulte:false,Lennar: false,Khov:false,"DR Horton":false}
			var filters =  {
					"first": false,
					"second": false,
					"third": false,
					"fourth":false,
					"fifth":false,
					"red":false,
					"blue":false,
					"Pulte":false,
					"Lennar":false,
					"Khov":false,
					"DR Horton":false,
					"Toll Brothers":false,
					"GL Homes":false,
					"Minto":false,
					"Stock":false,
					"Kolter":false,
					"Centerline-Tapestry":false,
					"CC Homes":false,
					"David Weekley":false,
					"DM Dean":false,
					"Atlantic Crystals":false,
					"SCI":false
				}
			function renderMap(locations, color,map) 
			{
				// set infobox to display the bubble while hover the mouse
				var myOptions = {
					disableAutoPan: false
					,maxWidth: 0
					,pixelOffset: new google.maps.Size(-20, 0)
					,zIndex: null
					,boxStyle: { 
					background: "url('http://www.geocodezip.com/images/tipbox90pad.gif') no-repeat"
						,opacity: 0.75
						,width: "45px"
					}
					,closeBoxURL: ""
					,isHidden: false
					,pane: "floatPane"
					,enableEventPropagation: false
				};
				// Toottip which will display the take rate of red and blue pins
				var tooltip = new InfoBox(myOptions);
				// information window used to display the information when click on the marker
				var infoWindow = new google.maps.InfoWindow({
					pixelOffset: new google.maps.Size(0, -5) //these are the offset values to be adjusted accordingly..
					,disableAutoPan: false
					,closeBoxMargin: "5px 5px 2px 2px"
					,boxStyle: {
						opacity: 0.95
					}
				});
				var marker, i;
				for (i = 0; i < locations.length; i++) 
				{  
					// use to diffirentiate the bubble with three different color
					var pin_category = '';
					// set propery which will help us for filteration
					var properties =  {
							"first": false,
							"second": false,
							"third": false,
							"fourth":false,
							"fifth":false,
							"red":false,
							"blue":false,
							"Pulte":false,
							"Lennar":false,
							"Khov":false,
							"DR Horton":false,
							"Toll Brothers":false,
							"GL Homes":false,
							"Minto":false,
							"Stock":false,
							"Kolter":false,
							"Centerline-Tapestry":false,
							"CC Homes":false,
							"David Weekley":false,
							"DM Dean":false,
							"Atlantic Crystals":false,
							"SCI":false
					}
					var key = locations[i].builder_name;
					properties[key] = true;
					if (color == 'red')
						properties.red = true
					else
						properties.blue = true
					if (locations[i].rate <= "20")
					{
						pin_category = "red-pin"
						properties.first = true
						color_new = "red"
					}
					else if(locations[i].rate > "20" && locations[i].rate <= "40")
					{
						properties.second = true
						pin_category = "black-pin"
						color_new = "yellow"
					}
					else if(locations[i].rate > "40" && locations[i].rate <= "60")
					{
						properties.third = true
						pin_category = "purple-pin"
						color_new = "blue"
					}
					else if(locations[i].rate > "60" && locations[i].rate <= "80")
					{
						properties.fourth = true
						pin_category = "yellow-pin"
						color_new = "ltblue"
					}
					else
					{
						pin_category = "orange-pin"
						properties.fifth = true
						color_new = "orange"
					}
					// create the marker for the google
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i].lat, locations[i].lang),
						map: map,
						icon: {
							url: 'http://maps.google.com/mapfiles/ms/icons/' + color_new + '-dot.png',
						},
						properties : properties 
					});
					marker.setVisible(false)
					// push the marker into array which will help us to toggle the pins
					gmarkers.push(marker);
					//---------------- Tooltip events ----------------
					google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
						var content = '<div id="'+pin_category+'" >\
						'+locations[i].rate+'%\
						</div>';
						return function() {
							tooltip.setContent(content);
							tooltip.open(map, marker);
						}
					})(marker, i));
					marker.addListener('mouseout', function() {
						tooltip.close();
					});
					//----------------- End tootltip events ----------------
					//------------------ Infowindow evnets -----------------
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						var onsite_collateral = locations[i].onsite_collateral == 0 ? 'N' : 'Y';
						var sunset = locations[i].sunset == 0 ? 'N' : 'Y';
						var trained = locations[i].trained == 0 ? 'N' : 'Y';
						var content = '<div>\
											<b>Master Community:</b> '+locations[i].master_community+'<br>\
											<b>Community:</b> '+locations[i].community+'<br>\
											<b>Region:</b> '+locations[i].region+'<br>\
											<b>County:</b> '+locations[i].county+'<br>\
											<b>City:</b> '+locations[i].city+'<br>\
											<b>Builder Name:</b> '+locations[i].builder_name+'<br>\
											<b>Dealer:</b> '+locations[i].dealer+'<br>\
											<b>Total Homes:</b> '+locations[i].total_homes+'<br>\
											<b>Empty Lots:</b> '+locations[i].empty_lots+'<br>\
											<b>Total PGT Homes:</b> '+locations[i].total_pgt_homes+'<br>\
											<b>Impact Rate:</b> '+locations[i].impact_rate+'<br>\
											<b>Incumbent Competitor:</b> '+locations[i].incumbent_competitor+'<br>\
											<b>Incumbent Rate:</b> '+locations[i].incumbent_rate+'<br>\
											<b>Onsite Collateral:</b> '+onsite_collateral+'<br>\
											<b>Trained:</b> '+trained+'<br>\
											<b>Sunset:</b> '+sunset+'<br>\
											<b>Last Action:</b> '+locations[i].last_action+'<br>\
											<b>Contact Info:</b> '+locations[i].contact_info+'<br>\
										</div>';
						return function() {
							tooltip.close();
							infoWindow.setContent(content);
							infoWindow.open(map, marker);
						}
					})(marker, i));
					//---------------------close Infowindow evennts----------------
				}
			}

			// function use to toggle the pins on google map
			$(document).on('click','.pins-btn', function() {
				for (i = 0; i<gmarkers.length; i++){
					if (gmarkers[i].getMap() != null) gmarkers[i].setMap(null);
							else gmarkers[i].setMap(map);
				}
				// $(this).toggleClass("active").next().slideToggle("fast");
				if ($.trim($(this).text()) === 'Hide pins') {
					$(this).removeClass("btn-danger");
					$(this).addClass("btn-success");
					$(this).text('Show pins');
				} else {
					$(this).addClass("btn-danger");
					$(this).removeClass("btn-success");
					$(this).text('Hide pins');        
				}
				return false; 
			});

			// perform operation on checkbox for filtering
			$('input[name=filter]').change(function (e) {
					// $('input[type="checkbox"]').not(this).prop('checked', false);    // make only checkbox avilable  
					map_filter(this.id); // manage the filter array properties
					// console.log(this.id, "this.id")
					// if ($(this).is(":checked"))
					// 	map_filter(this.id, true); // manage the filter array properties
					// else
					// 	map_filter(this.id, false); // manage the filter array properties
					// filter_markers(); // filter the markers
					filter_markers_new();
			});

			var filter_markers_new = function()
			{
				set_filters = get_set_options()
				for (i = 0; i < gmarkers.length; i++) 
				{
					marker = gmarkers[i];
					// start the filter check assuming the marker will be displayed
					// if any of the required features are missing, set 'keep' to false
					// to discard this marker
					keep = false
					// flag = true
					for (opt=0; opt<set_filters.length; opt++) 
					{
						// console.log("set_filters[opt]",set_filters[opt])
						if (marker.properties[set_filters[opt]]) 
							keep = true;
					}
					marker.setVisible(keep)
				}
			}

			// set the filter propertied
			var map_filter = function(id_val) {
				// filters = {first:false, second:false, third:false, red:false, blue:false}
				if (filters[id_val] ) 
					filters[id_val] = false
				else
					filters[id_val] = true
			}

			// get the options for filters
			var get_set_options = function() {
				ret_array = []
				for (option in filters) {
					if (filters[option]) 
						ret_array.push(option)
				}
				return ret_array;
			}

		</script>

	</body>
</html>

