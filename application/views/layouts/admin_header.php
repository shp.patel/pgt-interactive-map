<head>
	<meta charset="utf-8" />
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>Smarty Admin</title>
	<meta name="description" content="" />
	<meta name="Author" content="Dorin Grigoras [www.stepofweb.com]" />
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext" rel="stylesheet" type="text/css" />
	<link href="<?=ASSET?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=ASSET?>/css/essentials_admin.css" rel="stylesheet" type="text/css" />
	<link href="<?=ASSET?>/css/layout_admin.css" rel="stylesheet" type="text/css" />
	<link href="<?=ASSET?>/css/color_scheme/green.css" rel="stylesheet" type="text/css" id="color_scheme" />
	<link href="<?=ASSET?>/css/layout-datatables.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo ASSET;?>css/sweetalert2.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo ASSET;?>css/style.css" rel="stylesheet" type="text/css" />
	<!-- Latest compiled and minified JavaScript -->
	<script type="text/javascript">var plugin_path = '<?=ASSET?>/plugins/';</script>
	<script type="text/javascript" src="<?=ASSET?>/plugins/jquery/jquery-2.2.3.min.js"></script>
	<script type="text/javascript" src="<?=ASSET?>/js/app.js"></script>
</head>
