<meta charset="utf-8" />
<title></title>
<meta name="Online booking" content="mooring" />
<meta name="Mooring booking" content="" />
<meta name="Dan" content="Book Mooring" />
<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400%7CRaleway:300,400,500,600,700%7CLato:300,400,400italic,600,700" rel="stylesheet" type="text/css" />
<link href="<?= ASSET; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= ASSET; ?>css/essentials.css" rel="stylesheet" type="text/css" />
<link href="<?= ASSET; ?>css/layout.css" rel="stylesheet" type="text/css" />
<link href="<?= ASSET; ?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?= ASSET; ?>css/responsive.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?= ASSET; ?>plugins/jquery/jquery-2.2.3.min.js"></script>
