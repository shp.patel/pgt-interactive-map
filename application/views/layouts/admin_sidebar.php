<aside id="aside">
	<!--
		Always open:
		<li class="active alays-open">

		LABELS:
			<span class="label label-danger pull-right">1</span>
			<span class="label label-default pull-right">1</span>
			<span class="label label-warning pull-right">1</span>
			<span class="label label-success pull-right">1</span>
			<span class="label label-info pull-right">1</span>
	-->
	<nav id="sideNav"><!-- MAIN MENU -->
		<ul class="nav nav-list">
			
			<li class="<?= ($this->uri->segment(1) == 'a_dashboard') ? "active" : '' ?> ">
				<a class="dashboard" href="<?=BASE_URL?>a_dashboard"><!-- warning - url used by default by ajax (if eneabled) -->
					<i class="main-icon fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li>
			<li class="<?= ($this->uri->segment(1) == 'a_contacts' || $this->uri->segment(1) == 'a_add' || $this->uri->segment(1) == 'a_contactEdit' || $this->uri->segment(1) == 'a_downloadView') ? 'active' : ''?>">
				<a class="dashboard" href="<?=BASE_URL?>a_contacts"><!-- warning - url used by default by ajax (if eneabled) -->
					<i class="main-icon fa fa-users"></i> <span>Contacts</span>
				</a>
			</li>
		</ul>
		<!-- SECOND MAIN LIST -->
	</nav>
	<span id="asidebg"><!-- aside fixed background --></span>
</aside>
