<!-- JAVASCRIPT FILES -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- <script async type="text/javascript" src="<?=ASSET?>plugins/styleswitcher/styleswitcher.js"></script> -->
<script src="<?php echo ASSET;?>js/sweetalert2.min.js"></script>

<script>        
	var site_url = "<?php echo site_url() ?>";
	var base_url = "<?php echo base_url() ?>";        
	var IMG = "<?php echo IMG ?>";        
	$(document).ready(function()
	{            
		var isset_error = "<?= $this->session->flashdata('flash_error'); ?>";
		var isset_success = "<?= $this->session->flashdata('flash_success'); ?>";
		var isset_warning = "<?= $this->session->flashdata('flash_warning'); ?>";
		if(isset_error !="")
			_toastr(isset_error,"top-right","error",false);
		if(isset_success !="")
			_toastr(isset_success,"top-right","success",false);
		if(isset_warning !="")
			_toastr(isset_warning,"top-right","info",false);                
	});  
</script>

