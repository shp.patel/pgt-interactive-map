<!doctype html>
<html lang="en-US">
	<div id="loader" style="display:none">
	</div>
	<?php $this->load->view('layouts/admin_header');?>
	<body>
		<!-- WRAPPER -->
		<div id="wrapper" class="clearfix">
			<?php $this->load->view('layouts/admin_sidebar');?>

			<!-- HEADER -->
			<header id="header">
				<!-- Mobile Button -->
				<button id="mobileMenuBtn"></button>
				<!-- Logo -->
				<span class="logo pull-left">
					<img src="<?=ASSET?>images/pgti-logo-lg.jpg" alt="admin panel" height="35" />
				</span>

				<nav>

					<!-- OPTIONS LIST -->
					<ul class="nav pull-right">

						<!-- USER OPTIONS -->
						<li class="dropdown pull-left">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<img class="user-avatar" alt="" src="<?=ASSET?>/images/noavatar.jpg" height="34" /> 
								<span class="user-name">
									<span class="hidden-xs">
										<?=$this->session->userdata("name")?> <i class="fa fa-angle-down"></i>
									</span>
								</span>
							</a>
							<ul class="dropdown-menu hold-on-click">
								<li><!-- settings -->
									<a href="<?=BASE_URL?>a_settings"><i class="fa fa-cogs"></i> Settings</a>
								</li>
								<li class="divider"></li>
								<li><!-- logout -->
									<a href="<?=BASE_URL?>a_logout"><i class="fa fa-power-off"></i> Log Out</a>
								</li>
							</ul>
						</li>
						<!-- /USER OPTIONS -->
					</ul>
					<!-- /OPTIONS LIST -->
				</nav>
			</header>
			<!-- /HEADER -->
			<!-- 
				MIDDLE 
			-->
			<section id="middle">
				<?= $body ?>
			</section>
			<!-- /MIDDLE -->
		</div>
	
		<?php $this->load->view('layouts/admin_footer');?>
	
	</body>
</html>
