<?php 
function p($array) {    
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}
function sendMail($email, $sub, $msg, $attach ='', $bcc='')
{       
    $CI = &get_instance();
    $CI->load->library('email');
    $configs = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => '465', //465
            'smtp_user' => 'demo.narolainfotech@gmail.com',            
            'smtp_pass' => '{PASSWORD_HERE}',
            'smtp_crypto' => 'ssl',
            'transport' => 'Smtp',
            'charset' => 'utf-8',
            'newline' => "\r\n",
            'headerCharset' => 'iso-8859-1',
            'mailtype' => 'html'
        );
    $CI->email->initialize($configs);
    $CI->email->from('demo.narolainfotech@gmail.com', 'Book Mooring');
    $CI->email->to($email);
    // if(trim($bcc) != ''){
    //     $CI->email->bcc($bcc);
    // }else{
    //     $CI->email->bcc('kim@narola.email');
    // }
    $CI->email->subject($sub);
    $CI->email->message($msg); 
    if($attach !=""){
        foreach($attach as $row)
        {
            $CI->email->attach($row['url']);
        }
    }    
    if($CI->email->send()){        
        return true;
    } else{
        // print_r($CI->email->print_debugger(), true);
        // echo $CI->email->print_debugger();exit;
        return false;
    }
}
/*
** User define datetime format function
*/
function mydatetime($date)
{
    if($date > 0)
    {
        $mydateformat = date("d M Y h:i A",strtotime($date));
        return $mydateformat;
    }else{
        return "-";
    }
}
/*
** User define date format function
*/
function mydate($date)
{
    if($date > 0)
    {
        $mydateformat = date("d M Y",strtotime($date));
        return $mydateformat;
    }else{
        return "-";
    }
}
/*
** User define datetime format function
*/
function mytime($date){
    if($date != ''){
        $mydateformat = date("h:i A",strtotime($date));
        return $mydateformat;
    }else{
        return "-";
    }
}
/**
*@uses common fucntion for image upload
*@param file-  input name
*@param uploadpath-  where you want to upload
*@param fname-  file name you want to give
**/
function imageUpload($file, $upload_path, $fname)
{
    $CI =& get_instance();
    $file_type = array("image/jpeg", "image/png", "image/jpg");        
    if(isset($_FILES[$file]["name"]) && $_FILES[$file]["name"]!="")
    {   
        $str = explode(".", trim($_FILES[$file]["name"]));        
        $temp = preg_split('/(?<=[a-z])(?=[0-9]+)/i',$str[0]);
        if (!is_dir($upload_path)) {
             mkdir($upload_path, 0777, TRUE);
             chmod($upload_path,0777);
         }
        // upload image code
        $config['upload_path'] = $upload_path;
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '1024'; // 1 MB
        $file_name = preg_replace('/[^A-Za-z0-9\-]/','',$fname);
        if($file_name =="")
            $file_name = date("Ymd_his");
        $ext = end($str);
        $config['file_name'] = $file_name;
        $config['overwrite'] = TRUE;
        $CI->load->library('upload',$config);
        $CI->upload->initialize($config);
        if(!$CI->upload->do_upload($file)) 
        {
            $data = $CI->upload->display_errors();
            $msg = $data;            
            $response['msg'] = $msg;
            $response['path']='';
        }
        else{            
            $new_filename = $file_name.'.'.$ext;
            $path = $new_filename;
            $response['msg']="success";
            $response['path']=$path;            
        }        
    } 
    else{
        $response['msg']="success";
        $response['path']='';            
    }
    return $response;
}
/**
 *@uses fucntion to process array of file type 
 *@param file - file name
 *@param upload_path - path to upload file
 *@param image_index - incase require to upload single image for array
 */
function uploadMultiple($file, $upload_path, $fname){    
    $CI = &get_instance();    
    $dataInfo = array();
    $files = $_FILES;
    $cpt = count($_FILES[$file]['name']);
    if($cpt > 0){
        for($i=0; $i<$cpt; $i++)
        {
            if (!is_dir($upload_path)) {
                 mkdir($upload_path, 0777, TRUE);
                 chmod($upload_path,0777);
             }
            $_FILES[$file]['name']= $files[$file]['name'][$i];
            $_FILES[$file]['type']= $files[$file]['type'][$i];
            $_FILES[$file]['tmp_name']= $files[$file]['tmp_name'][$i];
            $_FILES[$file]['error']= $files[$file]['error'][$i];
            $_FILES[$file]['size']= $files[$file]['size'][$i];
            
            $config = array();
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['overwrite']     = FALSE;
            $filename = date("ymd_his").$i;
            $config['file_name'] = $filename;
            $config['overwrite'] = TRUE;
            $CI->load->library('upload',$config);
            $CI->upload->initialize($config);
            if(!$CI->upload->do_upload($file))
            {
                $data = $CI->upload->display_errors();
                $msg = $data;
                $response[$i]['msg'] = $msg;
                $response[$i]['path'] = '';
                $response[$i]['org_name'] = '';
            }
            else{            
                $dataInfo[] = $CI->upload->data();
                $new_filename = $dataInfo[$i]['file_name'];
                $path = $dataInfo[$i]['file_name'];
                $response[$i]['msg']="success";
                $response[$i]['path']=$path;
                $response[$i]['org_name']= $files[$file]['name'][$i];
            }        
        }
    }else{
        $response[0]['msg']="success";
        $response[0]['path']= '';
    }
    return $response;
}
/**
 *@uses function to create random string function
 */
function generateRandomString($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}?>