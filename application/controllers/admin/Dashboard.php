<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct() 
	{
        parent::__construct();
		$this->load->model('admin_m');
		$this->load->model('map_m');
		if ($this->admin_m->u_loggedin() == FALSE) {
            redirect('a_login');
            exit;
        }
	}

	/** 
	 *@uses function to load home page
	 */
	public function index()
	{
		$relation = array(
			"fields" => "*",
			"conditions" => " is_delete = 0 AND total_pgt_homes = 0",
		);
		$data['number_of_red_pins'] = $this->map_m->get_relation('', $relation, true);
		$relation = array(
			"fields" => "*",
			"conditions" => "is_delete = 0 AND total_pgt_homes > 0",
		);
		$data['number_of_blue_pins'] = $this->map_m->get_relation('', $relation, true);
		$relation = array(
			"fields" => "AVG(incumbent_rate) as avg_incumbent_rate",
			"conditions" => "is_delete = 0",
		);
		$data['average_incumbent_rate'] = $this->map_m->get_relation('', $relation)[0]['avg_incumbent_rate'];
		$relation = array(
			"fields" => "AVG(impact_rate) as avg_impact_rate, region",
			"conditions" => "is_delete = 0",
		);
		$data['average_impact_rate'] = $this->map_m->get_relation('', $relation)[0]['avg_impact_rate'];
		$relation = array(
			"fields" => "AVG(incumbent_rate) as avg_incumbent_rate, region ,
			CASE
				WHEN region = 'southwest' THEN 'South West'
				WHEN region = 'southeast' THEN 'South East'
				WHEN region = 'centraleast' THEN 'Central East'
				WHEN region = 'centralwest' THEN 'Central West'
				WHEN region = 'northeast' THEN 'North East'
				WHEN region = 'northwest' THEN 'North West'
			END AS region",
			"conditions" => "is_delete = 0 AND total_pgt_homes = 0",
			"GROUP_BY" => 'region'
		);
		$data['region_wise_red_pins'] = $this->map_m->get_relation('', $relation);
		$relation = array(
			"fields" => "AVG(impact_rate) as avg_impact_rate, region,
				CASE
					WHEN region = 'southwest' THEN 'South West'
					WHEN region = 'southeast' THEN 'South East'
					WHEN region = 'centraleast' THEN 'Central East'
					WHEN region = 'centralwest' THEN 'Central West'
					WHEN region = 'northeast' THEN 'North East'
					WHEN region = 'northwest' THEN 'North West'
				END AS region",
			"conditions" => "is_delete = 0 AND total_pgt_homes > 0",
			"GROUP_BY" => 'region'
		);
		$data['region_wise_blue_pins'] = $this->map_m->get_relation('', $relation);
		$this->template->view('admin_template', 'pages/admin/dashboard', $data);
	}

	/** 
	 *@uses function to load change password page
	 */
	public function settings()
	{
		$this->template->view('admin_template', 'pages/admin/settings');
	}

	/** 
	 *@uses function change the password for admin
	*/
	public function changePassword()
	{
		$new_password = $this->input->post('new_password');
		$confirm_password = $this->input->post('confirm_password');
		if ($new_password === $confirm_password)
		{
			$array['password'] = md5($new_password);
			$result = $this->admin_m->save($array, $this->session->userdata('user_id'));
			if ($result)
				$this->session->set_flashdata('flash_success',"Your password has been changed successfully");
			else
				$this->session->set_flashdata('flash_error',"Something happens wrong");
		}
		else
			$this->session->set_flashdata('flash_error',"Password and Confirm password confirmation does not match");
		redirect('a_settings');
	}

}
