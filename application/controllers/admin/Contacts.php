<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends MY_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->model('admin_m');
		$this->load->model('map_m');
		if ($this->admin_m->u_loggedin() == FALSE) {
            redirect('a_login');
            exit;
        }
	}

	/** 
	 *@uses function to load home page
	 */
	public function index()
	{
		$this->template->view('admin_template', 'pages/admin/contacts');
	}

	/** 
	 *@uses function to load the contact data json through indexjson
	 */
	public function contactsjson()
	{
		$sColumns =  array('master_community','community','region','city', 'builder_name', 'dealer', 'street', 'pgt_product', 'total_homes', 'empty_lots', 'total_pgt_homes', 'impact_rate', 'incumbent_competitor','incumbent_rate','onsite_collateral','trained','name','contact_info','last_action','sunset');
		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $sColumns[$this->input->post('order')[0]['column']];
		$dir = $this->input->post('order')[0]['dir'];
		$relation = array(
			"fields" => "*",
			"conditions" => "is_delete = 0",
		);
		$totalData = $this->map_m->get_relation('', $relation, true);
		$totalFiltered = $totalData; 
		$sWhere = "";
		$search = $this->input->post('search')['value'];
        if ($search != "") {
            $sWhere .= " (";
            for ($i = 0; $i < count($sColumns); $i++)
                $sWhere .= $sColumns[$i] . " LIKE '%" . $search . "%' OR ";
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ') ';
        }
		if (empty($search))
			$maps = $this->map_m->all($limit,$start,$order,$dir);
		else {
			$maps =  $this->map_m->all($limit,$start,$order,$dir,$sWhere);
			$totalFiltered = count($maps);
		}
		$data = array();
		if(!empty($maps))
		{
			foreach ($maps as $map)
				$data[] = $map;
		}
		$json_data = array(
			"draw"            => intval($this->input->post('draw')),  
			"recordsTotal"    => intval($totalData),  
			"recordsFiltered" => intval($totalFiltered), 
			"data"            => $data   
		);
		echo json_encode($json_data);
	}

	/** 
	 *@uses function to load the contact data json through indexjson
	*/
	public function add()
	{
		$this->template->view('admin_template', 'pages/admin/add');
	}

	/** 
	 *@uses function to load the contact data json through indexjson
	 */
	public function save()
	{
		$this->form_validation->set_rules($this->map_m->rules);
        if ($this->form_validation->run() == FALSE) 
			$this->template->view('admin_template', 'pages/admin/add');
		else
		{
			$data = $this->map_m->array_from_post(array('master_community','community','county','region','city', 'builder_name', 'dealer', 'street', 'pgt_product', 'total_homes', 'empty_lots', 'total_pgt_homes', 'incumbent_competitor','incumbent_rate','name','contact_info','last_action'));
			$data['onsite_collateral'] = !empty($this->input->post('onsite_collateral')) ? $this->input->post('onsite_collateral') : '1';
			$data['trained'] = !empty($this->input->post('trained')) ? $this->input->post('trained') : '1';
			$data['sunset'] = !empty($this->input->post('sunset')) ? $this->input->post('sunset') : '1';
			// if impact rate is 0 than we will calculate it based on total_pgt_homes/empty_lots
			$impact_rate = 0;
			if ($this->input->post('impact_rate') <> 0 && $this->input->post('impact_rate') != '')
				$impact_rate = $this->input->post('impact_rate');
			else if( $this->input->post('total_pgt_homes') > 0 && $this->input->post('empty_lots') > 0)
				$impact_rate = $this->input->post('total_pgt_homes')/$this->input->post('empty_lots');
			$data['impact_rate'] = $impact_rate;
			$result = $this->get_address($this->input->post("street"));
			if ($result <> false)
			{
				$data['lat'] = $result['latitude'];
				$data['lang'] = $result['longitude'];
			}
			else{
				$data['lat'] = NULL;
				$data['lang'] = NULL;
			}
			if (!empty($this->input->post("id")))
			{
				$result = $this->map_m->save($data,$this->input->post("id"));
				$message = "Data updated successfully!!!";
			}
			else
			{
				$message = "Data added successfully!!!";
				$result = $this->map_m->save($data);
			}
            if ($result) {
                $this->session->set_flashdata('flash_success', $message);
                redirect('a_contacts');
            } else {
                $this->session->set_flashdata('flash_error', 'Something went wrong.');
                redirect('a_contacts');
            }
		}
	}

	/** 
     *@uses function used for the custom validation
	*/
	public function _maximumCheck($num)
	{
		if ($num > 100 || $num < 0)
		{
			$this->form_validation->set_message(
							'_maximumCheck',
							'The %s field must have value between 0 to 100'
						);
			return FALSE;
		}
		else
			return TRUE;
	}

	/** 
	 *@uses function to load the contact data json through indexjson
	*/
	public function edit($id = '')
	{
		$data['result'] = $this->map_m->get($id);
		if (empty($data['result']))
		{
			$this->session->set_flashdata('flash_error', 'Data not found.');
			$this->index();
		}
		else
			$this->template->view('admin_template', 'pages/admin/add', $data);
	}
	
	/** 
	 *@uses function to load the contact data json through indexjson
	*/
	public function delete()
	{
		$id = $this->input->post("id");
		$result = $this->map_m->get($id);
		if (empty($result))
			return  $this->output
				->set_content_type('application/json')
				->set_output(json_encode(array('success' => false, 'message' => 'Something happens worng')));
		else
			$this->map_m->delete($id);
			return  $this->output
				->set_content_type('application/json')
				->set_output(json_encode(array('success' => true, 'message' => 'Data deleted successfully')));
		
	}

		/** 
	 *@uses function render the view fro validation
	*/
	public function loadDownloadView()
	{
		$this->template->view('admin_template', 'pages/admin/download');
	}

	/** 
	 *@uses function to download the sample csv file
	*/
	public function downloadCSV()
	{
		$this->session->set_flashdata("flash_warning","You can add data into downloded file as per requirement. But keep other data remains same");
		$file_name = time().'.csv';
		$data = array ("Master Community (If applicable)","Community Name",	"Region (also called Territory or District or Division)","County","City","Builder Name","Dealer(s)","Community Street Address","Applicable PGT Product","Total Number of Homes","Empty Lots","Number of homes using PGT Product","Impact Take Rate","Incumbent Competitor","Incumbent Take Rate","On Site Collateral?","Trained?","Contact Name","Contact Info (Tel, Email)","Last Action","Sunset?");
		# output headers so that the file is downloaded rather than displayed
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=$file_name");
		# Disable caching - HTTP 1.1
		header("Cache-Control: no-cache, no-store, must-revalidate");
		# Disable caching - HTTP 1.0
		header("Pragma: no-cache");
		# Disable caching - Proxies
		header("Expires: 0");
		# Start the ouput
		$output = fopen("php://output", "w");
		# Set column name
		fputcsv($output, $data); // here you can change delimiter/enclosure
		# Close the stream off
		fclose($output);
	}

	/** 
	 * @uses function to import the csv data
	*/
	public function import_csv()
	{
		$data = $this->input->post("data");
		// echo "<pre>";
		// print_r($data);
		// die;
		$errors = [];
		$columns =  array('master_community','community','county','region','city', 'builder_name', 'dealer', 'street', 'pgt_product', 'total_homes', 'empty_lots', 'total_pgt_homes', 'impact_rate', 'incumbent_competitor','incumbent_rate','onsite_collateral','trained','name','contact_info','last_action','sunset');
		$column_names = array("Master Community (If applicable)","Community Name",'County',"Region (also called Territory or District or Division)","City","Builder Name","Dealer(s)","Community Street Address","Applicable PGT Product","Total Number of Homes","Empty Lots","Number of homes using PGT Product","Impact Take Rate","Incumbent Competitor","Incumbent Take Rate","On Site Collateral?","Trained?","Contact Name","Contact Info (Tel, Email)","Last Action","Sunset?");
		if(count($data) > 1)
			$this->db->query('UPDATE tbl_map_details SET is_delete = 1'); // update the record
		foreach($data as $k=>$record)
		{
			$array = [];
			if ($k > 0)
			{
				if (!empty($record))
				{
					$ex_record = explode(",",$record);
					$error_col = [];
					foreach($columns as $key=>$col)
					{
						// if ($col == 'master_community' || $col == 'community' || $col == 'county' || $col == "region" || $col == 'country' || $col == 'city' || $col == 'builder_name' || $col == 'dealer' || $col == 'pgt_product' || $col == 'incumbent_competitor' || $col == 'name')
						// {
						// 	$result = preg_match('/^[a-zA-Z ]+$/i', trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'street')
						// {
						// 	$result = preg_match('/^[a-zA-Z0-9 ]+$/i', trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'contact_info')
						// {
						// 	$result = preg_match("/^[()-_@a-z A-Z0-9\\/\\\\.'\"]+$/",trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'total_homes' || $col == 'empty_lots' || $col == 'total_pgt_homes')
						// {
						// 	$result = is_numeric(trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'impact_rate' || $col == 'incumbent_rate')
						// {
						// 	$result = is_numeric(trim($ex_record[$key])) && trim($ex_record[$key] )> 0 && trim($ex_record[$key] )<= 100;
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'onsite_collateral' || $col == 'trained' || $col == 'sunset')
						// {
						// 	$arrayy = array('Y','N','Yes','No','yes','no');
						// 	$result = in_array(trim($ex_record[$key]), $arrayy);
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'last_action')
						// {
						// 	$result = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						if ($col == 'community' || $col == 'builder_name')
						{
							$result = preg_match('/^[\/\'\!@#_\%\(\)\-\+=\\\\a-z@A-Z ]+$/i', trim($ex_record[$key]));
							if ($result == 0)
								array_push($error_col, $column_names[$key]);
							else
								$array[$col] = trim($ex_record[$key]);
						}
						else if ($col == 'street')
						{
							$string = str_replace('"', ' ',  trim($ex_record[$key]));
							// echo $string;
							// echo "<br>";
							$result = preg_match('/^["a-z.A-Z0-9@ ]+$/i',$string);
							if ($result == 0)
								array_push($error_col, $column_names[$key]);
							else
								$array[$col] = trim($string);
						}
						else if ($col == 'last_action')
						{
							if (!empty($ex_record[$key]))
							{
								$result = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",trim($ex_record[$key]));
								if ($result == 0)
									array_push($error_col, $column_names[$key]);
								else
									$array[$col] = date('Y-m-d H:i:s');
							}
							else{
								$array[$col] = date('Y-m-d H:i:s');
							}
						}
						else{
							$string = str_replace('"', ' ',  trim($ex_record[$key]));
							$array[$col] = trim($string);
						}
						// else
						// {
						// 	$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'contact_info')
						// {
						// 	$result = preg_match("/^[()-_@a-z A-Z0-9\\/\\\\.'\"]+$/",trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'total_homes' || $col == 'empty_lots' || $col == 'total_pgt_homes')
						// {
						// 	$result = is_numeric(trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'impact_rate' || $col == 'incumbent_rate')
						// {
						// 	$result = is_numeric(trim($ex_record[$key])) && trim($ex_record[$key] )> 0 && trim($ex_record[$key] )<= 100;
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'onsite_collateral' || $col == 'trained' || $col == 'sunset')
						// {
						// 	$arrayy = array('Y','N','Yes','No','yes','no');
						// 	$result = in_array(trim($ex_record[$key]), $arrayy);
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
						// else if ($col == 'last_action')
						// {
						// 	$result = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",trim($ex_record[$key]));
						// 	if ($result == 0)
						// 		array_push($error_col, $column_names[$key]);
						// 	else
						// 		$array[$col] = trim($ex_record[$key]);
						// }
					}
					// echo "<pre>";
					// print_r($array);
					if (empty($array['impact_rate']))
					{
						if (is_numeric(trim($array['total_pgt_homes'])) >= 1 AND is_numeric(trim($array['empty_lots'])) > 0)
						{
							$array['impact_rate'] = trim($array['total_pgt_homes']) / trim($array['empty_lots']);
						}
						else
						{
							$array['impact_rate'] = 0;
						}
					}
					// print_r($array);
					$error_string = count($error_col) > 0 ? "'".implode(',', $error_col)."'" : '';
					!empty($error_string) ? array_push($errors,"Could not find valid information for ".$error_string." for ".$k." row") : '';
					if (!empty($array['street']) && empty($error_string))
					{
						$result = $this->get_address($array['street']);
						if ($result <> false)
						{
							$array['lat'] = $result['latitude'];
							$array['lang'] = $result['longitude'];
						}
						else{
							$array['lat'] = NULL;
							$array['lang'] = NULL;
						}
						$this->map_m->save($array);
					}
				}
			}
		}
		$final_error_string = implode(",\n", $errors);
		return $this->output->set_content_type("application/json")->set_output(json_encode(array("status"=>"yes","errors"=>$final_error_string)));
	}

	public function get_address($address = '')
	{
		$geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.GOOGLE_API_KEY);
		$geo = json_decode($geo, true); // Convert the JSON to an array
		if (isset($geo['status']) && ($geo['status'] == 'OK')) {
			$array['latitude'] = $geo['results'][0]['geometry']['location']['lat']; // Latitude
			$array['longitude'] = $geo['results'][0]['geometry']['location']['lng']; // Longitude
			return $array;
		}
		else{
			return false;
		}
	}
}
