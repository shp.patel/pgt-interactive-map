<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Security extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('admin_m');
	}
	
	/** 
	 *@uses function to load home page
	 */
	public function login()
	{
		if ($this->session->userdata('u_loggedin') == TRUE) {
            redirect('a_contacts');
            exit;
		}
	
		$this->load->view('pages/admin/login');
	}

	/** 
	 *@uses function to check login credentials is correct or not
	 */
	public function save()
	{
		$email_id = $this->input->post('email');
		$password = md5($this->input->post('password'));
        if ($this->admin_m->login($email_id, $password) == FALSE)
        {
            $this->session->set_flashdata('flash_error',"Username and password combination doesn't exists");
            redirect('a_login');
            exit;
        }
		redirect("a_contacts");
	}

	/** 
	 *@uses function to logout the admin
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("a_login");
	}
}
