<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() 
	{
        parent::__construct();
		$this->load->model('map_m');
	}

	/** 
	 *@uses function to load home page
	 */
	public function index()
	{
		$main_array = array("Atlantic Crystals",
			"CC Homes",
			"Centerline-Tapestry",
			"David Weekley",
			"DM Dean",
			"DR Horton",
			"GL Homes",
			"Khov",
			"Kolter",
			"Lennar",
			"Minto",
			"Pulte",
			"SCI",
			"Stock",
			"Toll Brothers");
		$relation = array(
			"fields" => "sum(total_homes) as total, builder_name",
			"conditions" => "is_delete = 0",
			"GROUP_BY" => 'builder_name'
		);
		$data['builder_with_total_homes'] = $this->map_m->get_relation('', $relation);
		$columns = array_column($data['builder_with_total_homes'], "builder_name");
		$diff_array = array_diff($main_array, $columns);
		$diff_with_avg_value = array();
		foreach($diff_array as $key=>$value)
		{
			array_push($diff_with_avg_value, array('total'=>0,'builder_name'=>$value));
		}
		$data['builders'] = array_merge($data['builder_with_total_homes'], $diff_with_avg_value);
		$this->template->view('wb_template', 'pages/home_page', $data);
	}

	/** 
	 *@uses function to load home page
	 */
	public function fetchRecord()
	{
		$data = [];
		$relation = array(
			"fields" => "community,lat,lang,total_pgt_homes,builder_name,total_homes,last_action,contact_info,incumbent_rate as rate,name,master_community,region,county,city,dealer,street,empty_lots,incumbent_competitor,onsite_collateral,trained,sunset,incumbent_rate,impact_rate",
			"conditions" => "lat IS NOT null AND lang IS NOT null AND is_delete = 0 AND total_pgt_homes = 0",
		);
		$data["red"] = $this->map_m->get_relation('', $relation);
		$relation = array(
			"fields" => "community, lat, lang, total_pgt_homes,builder_name,total_homes,last_action,contact_info,impact_rate as rate,name,master_community,region,county,city,dealer,street,empty_lots,incumbent_competitor,onsite_collateral,trained,sunset,incumbent_rate,impact_rate",
			"conditions" => "lat IS NOT null AND lang IS NOT null AND is_delete = 0 AND total_pgt_homes > 0",
		);
		$data["blue"] = $this->map_m->get_relation('', $relation);
		echo json_encode($data);
	}
	    
}
